class CreateStocks < ActiveRecord::Migration[7.0]
  def change
    create_table :stocks do |t|
      t.belongs_to :star
      t.integer :stock_code, null: false
      t.integer :status_code, null: false, default: 0
      t.integer :price_decs
    end
  end
end
