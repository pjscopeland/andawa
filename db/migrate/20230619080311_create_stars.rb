class CreateStars < ActiveRecord::Migration[7.0]
  def change
    create_table :stars do |t|
      t.belongs_to :sector, null: false, foreign_key: true
      t.integer :galactic_id
      t.string :name
      t.integer :x
      t.integer :y
      t.integer :z
      t.integer :multiple
      t.integer :stardesc
      t.integer :population
      t.integer :danger
      t.integer :tech_level
      t.integer :chance_cargo_check
      t.integer :government
      t.integer :allegiance
      t.integer :federal_military_strength
      t.integer :imperial_military_strength
      t.string :short_description
      t.string :long_description
      t.boolean :permit_required

      t.index :population
    end
  end
end
