# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_06_20_065834) do
  create_table "sectors", force: :cascade do |t|
    t.integer "x"
    t.integer "y"
  end

  create_table "stars", force: :cascade do |t|
    t.integer "sector_id", null: false
    t.integer "galactic_id"
    t.string "name"
    t.integer "x"
    t.integer "y"
    t.integer "z"
    t.integer "multiple"
    t.integer "stardesc"
    t.integer "population"
    t.integer "danger"
    t.integer "tech_level"
    t.integer "chance_cargo_check"
    t.integer "government"
    t.integer "allegiance"
    t.integer "federal_military_strength"
    t.integer "imperial_military_strength"
    t.string "short_description"
    t.string "long_description"
    t.boolean "permit_required"
    t.index ["sector_id"], name: "index_stars_on_sector_id"
  end

  create_table "stocks", force: :cascade do |t|
    t.integer "star_id"
    t.integer "stock_code", null: false
    t.integer "status_code", default: 0, null: false
    t.integer "price_decs"
    t.index ["star_id"], name: "index_stocks_on_star_id"
  end

  add_foreign_key "stars", "sectors"
end
