require 'rails_helper'

RSpec.describe Stock do
  let!(:star) { Star.lookup('Sol', 0, 0) } # This creates 10 stars and 279 stocks
  let(:water) { star.stocks.water.first }
  let(:nerve_gas) { star.stocks.nerve_gas.first }

  describe 'scopes' do
    it('by type') { expect(described_class.nerve_gas).to all(be_nerve_gas) }
    it('by status') { expect(described_class.major_import).to all(be_major_import) }
  end

  describe '#stock_type' do
    it { expect(water.stock_type).to eq :water }
    it { expect(nerve_gas.stock_type).to eq :nerve_gas }
  end

  describe '#stock_type_text' do
    it { expect(water.stock_type_text).to eq 'Water' }
    it { expect(nerve_gas.stock_type_text).to eq 'Nerve Gas' }
  end

  describe '#status' do
    it { expect(water.status).to eq :unlisted }
    it { expect(nerve_gas.status).to eq :illegal_goods }
  end

  describe '#status_text' do
    it { expect(water.status_text).to eq 'Unlisted' }
    it { expect(nerve_gas.status_text).to eq 'Illegal Goods' }
  end

  describe '#<status>?' do
    it { expect(water).to be_unlisted }
    it { expect(water).not_to be_major_import }
    it { expect(water).not_to be_minor_import }
    it { expect(water).not_to be_major_export }
    it { expect(water).not_to be_minor_export }
    it { expect(water).not_to be_illegal_goods }
    it { expect(nerve_gas).not_to be_unlisted }
    it { expect(nerve_gas).not_to be_major_import }
    it { expect(nerve_gas).not_to be_minor_import }
    it { expect(nerve_gas).not_to be_major_export }
    it { expect(nerve_gas).not_to be_minor_export }
    it { expect(nerve_gas).to be_illegal_goods }
  end
end
