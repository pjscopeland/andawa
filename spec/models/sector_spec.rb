require 'rails_helper'

RSpec.describe Sector do
  let(:x) { 0 }
  let(:y) { 0 }
  let(:sector) { described_class.create(x: x, y: y) }

  describe 'generation' do
    subject(:stars) {
      sector.stars.pluck(:name, :x, :y, :z)
    }

    context 'with a predefined sector' do
      let(:x) { 0 }
      let(:y) { 0 }
      let(:expected) {
        [
          ['Sol', 0, 16, -54],
          ['Alpha Centauri', -15, -52, -52],
          ['Wolf 359', 58, -19, -127],
          ['Lalande 21185', 47, 40, -127],
          ['UV Ceti', 27, 38, 83],
          ['Ross 128', 48, -58, -127],
          ['Tau Ceti', 41, 52, 127],
          ['LET 118', 14, 52, 127],
          ['Wolf 424', 18, -56, -127],
          ['CD-37º15492', -38, -24, 127],
          ['Lalande 25372', -55, -36, -127],
        ]
      }

      it { expect(stars).to eq(expected) }
    end

    context 'with a generated sector' do
      let(:x) { 0 }
      let(:y) { -4 }
      let(:expected) {
        [
          ['Sohoa', 41, 49, -112],
          ['Facece', 43, -5, 97],
          ['Vequess', -57, -18, 77],
        ]
      }

      it { expect(stars).to eq(expected) }
    end
  end
end
