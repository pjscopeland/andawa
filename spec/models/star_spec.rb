require 'rails_helper'

RSpec.describe Star do
  let!(:sol) { described_class.lookup('Sol', 0, 0) }

  describe '.generate_galactic_id' do
    subject { described_class.generate_galactic_id(Frontier::RELATIVE_X_OFFSET, Frontier::RELATIVE_Y_OFFSET, 0) }

    it { is_expected.to eq 0x02A49718 }
  end

  describe '.lookup' do
    it('returns a pre-generated, predefined star') { expect(described_class.lookup('Sol')).to eq sol }
  end

  describe '.search' do
    subject { described_class.search(name) }

    let(:name) { 'Facece' }

    it { # rubocop:todo RSpec/ExampleLength
      is_expected.to eq [
        [0, 0],
        [1, 0],
        [1, 1],
        [0, 1],
        [-1, 1],
        [-1, 0],
        [-1, -1],
        [0, -1],
        [1, -1],
        [2, 0],
        [2, 1],
        [2, 2],
        [1, 2],
        [0, 2],
        [-1, 2],
        [-2, 2],
        [-2, 1],
        [-2, 0],
        [-2, -1],
        [-2, -2],
        [-1, -2],
        [0, -2],
        [1, -2],
        [2, -2],
        [2, -1],
        [3, 0],
        [3, 1],
        [3, 2],
        [3, 3],
        [2, 3],
        [1, 3],
        [0, 3],
        [-1, 3],
        [-2, 3],
        [-3, 3],
        [-3, 2],
        [-3, 1],
        [-3, 0],
        [-3, -1],
        [-3, -2],
        [-3, -3],
        [-2, -3],
        [-1, -3],
        [0, -3],
        [1, -3],
        [2, -3],
        [3, -3],
        [3, -2],
        [3, -1],
        [4, 0],
        [4, 1],
        [4, 2],
        [4, 3],
        [4, 4],
        [3, 4],
        [2, 4],
        [1, 4],
        [0, 4],
        [-1, 4],
        [-2, 4],
        [-3, 4],
        [-4, 4],
        [-4, 3],
        [-4, 2],
        [-4, 1],
        [-4, 0],
        [-4, -1],
        [-4, -2],
        [-4, -3],
        [-4, -4],
        [-3, -4],
        [-2, -4],
        [-1, -4],
        [0, -4],
      ]
    }
  end

  describe '#exact_coordinates' do
    subject { sol.exact_coordinates }

    it { is_expected.to eq [0.0, 0.125, -0.421875] }
  end

  describe '#stocks' do
    subject { star.stocks.map { |s| s.attributes.except('id', 'star_id') } }

    context 'for Sol' do
      let(:star) { sol }

      it { # rubocop:todo RSpec/ExampleLength
        is_expected.to eq [
          {'status_code' => 0, 'stock_code' => 'water', 'price_decs' => 9},
          {'status_code' => 0, 'stock_code' => 'liquid_oxygen', 'price_decs' => 100},
          {'status_code' => 10, 'stock_code' => 'grain', 'price_decs' => 384},
          {'status_code' => 11, 'stock_code' => 'fruit_and_veg', 'price_decs' => 920},
          {'status_code' => 11, 'stock_code' => 'animal_meat', 'price_decs' => 1286},
          {'status_code' => 2, 'stock_code' => 'synthetic_meat', 'price_decs' => 203},
          {'status_code' => 14, 'stock_code' => 'liquor', 'price_decs' => 3475},
          {'status_code' => 129, 'stock_code' => 'narcotics', 'price_decs' => 12_643},
          {'status_code' => 0, 'stock_code' => 'medicines', 'price_decs' => 5626},
          {'status_code' => 0, 'stock_code' => 'fertilizer', 'price_decs' => 149},
          {'status_code' => 137, 'stock_code' => 'animal_skins', 'price_decs' => 15_840},
          {'status_code' => 137, 'stock_code' => 'live_animals', 'price_decs' => 18_710},
          {'status_code' => 129, 'stock_code' => 'slaves', 'price_decs' => 20_268},
          {'status_code' => 14, 'stock_code' => 'luxury_goods', 'price_decs' => 11_860},
          {'status_code' => 3, 'stock_code' => 'heavy_plastics', 'price_decs' => 365},
          {'status_code' => 3, 'stock_code' => 'metal_alloys', 'price_decs' => 267},
          {'status_code' => 2, 'stock_code' => 'precious_metals', 'price_decs' => 17_226},
          {'status_code' => 2, 'stock_code' => 'gem_stones', 'price_decs' => 29_735},
          {'status_code' => 0, 'stock_code' => 'minerals', 'price_decs' => 70},
          {'status_code' => 1, 'stock_code' => 'hydrogen_fuel', 'price_decs' => 98},
          {'status_code' => 1, 'stock_code' => 'military_fuel', 'price_decs' => 501},
          {'status_code' => 129, 'stock_code' => 'hand_weapons', 'price_decs' => 10_149},
          {'status_code' => 129, 'stock_code' => 'battle_weapons', 'price_decs' => 12_095},
          {'status_code' => 129, 'stock_code' => 'nerve_gas', 'price_decs' => 16_477},
          {'status_code' => 1, 'stock_code' => 'industrial_parts', 'price_decs' => 1289},
          {'status_code' => 5, 'stock_code' => 'computers', 'price_decs' => 5190},
          {'status_code' => 2, 'stock_code' => 'air_processors', 'price_decs' => 2094},
          {'status_code' => 5, 'stock_code' => 'farm_machinery', 'price_decs' => 837},
          {'status_code' => 4, 'stock_code' => 'robots', 'price_decs' => 8578},
          {'status_code' => 129, 'stock_code' => 'radioactives', 'price_decs' => -83},
          {'status_code' => 2, 'stock_code' => 'rubbish', 'price_decs' => -7},
        ]
      }
    end

    context 'for an uninhabited system' do
      let(:star) { described_class.lookup('Adaze', 3, 1) }

      it { is_expected.to eq [] }
    end
  end

  describe '#bodies', skip: 'not implemented' do
    subject { star.bodies }

    context 'in a predefined system' do
      let(:star) { described_class.find('Alpha Centauri', 0, 0) }
      let(:expected) {
        [
          {
            'name' => 'Alpha Centauri A,B',
            'type' => 'Binary system',
            'ports' => [],
            'children' => [
              {
                'name' => 'Alpha Centauri A',
                'type' => "Type 'G' yellow star",
                'distance' => 23.340,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Alpha Centauri B',
                'type' => "Type 'K' orange star",
                'distance' => 23.340,
                'ports' => [],
                'children' => [],
              },
            ],
          },
          {
            'name' => 'Lagrange',
            'type' => 'Large gas giant',
            'distance' => 23.341,
            'ports' => [],
            'children' => [
              {
                'name' => '2042 L1',
                'type' => 'Small barren sphere of rock',
                'distance' => 0.04,
                'ports' => [],
                'children' => [],
              },
            ],
          },
          {
            'name' => '2071 AC3',
            'type' => 'Small gas giant',
            'distance' => 201.864,
            'ports' => [],
            'children' => [
              {
                'name' => nil,
                'type' => 'Small barren sphere of rock',
                'distance' => 0.01,
                'ports' => [],
                'children' => [],
              },
            ],
          },
          {
            'name' => 'Proxima Centauri',
            'type' => "Type 'M' flare star",
            'distance' => 963.360,
            'ports' => [],
            'children' => [
              {
                'name' => 'Eden',
                'type' => 'World with water weather system and corrosive atmosphere',
                'distance' => 0.89,
                'ports' => [],
                'children' => [
                  {
                    'name' => 'Eden Station',
                    'type' => 'Orbital trading post',
                    'distance' => 0.0,
                    'ports' => [],
                    'children' => [],
                  },

                ],
              },
              {
                'name' => '2045 PC2',
                'type' => 'Small barren sphere of rock',
                'distance' => 0.224,
                'ports' => [],
                'children' => [],
              },
            ],
          },
        ]
      }

      it { is_expected.to eq expected }
    end

    context 'in a generated system' do
      let(:star) { described_class.find('Facece', 0, -4) }
      let(:expected) {
        [
          {
            'name' => 'Facece',
            'type' => "Type 'F' white star",
            'distance' => 0.0,
            'ports' => [],
            'children' => [
              {
                'name' => 'Facece 1',
                'type' => 'Barren rocky planetoid',
                'distance' => 0.220,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Facece 2',
                'type' => 'Rocky planet with a thin atmosphere',
                'distance' => 0.323,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => "Matthews's Hole",
                'type' => 'Small barren sphere of rock',
                'distance' => 0.467,
                'ports' => ['Schneider'],
                'children' => [],
              },
              {
                'name' => "Coates's Mine",
                'type' => 'Rocky planet with a thin atmosphere',
                'distance' => 0.697,
                'ports' => ['Köhl Depot'],
                'children' => [
                  {
                    'name' => 'Simpson Depot',
                    'type' => 'Orbital station',
                    'distance' => 0.0,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => 'Facece 5',
                'type' => 'Rocky world with a thick corrosive atmosphere',
                'distance' => 1.56,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Facece 6',
                'type' => 'Rocky world with a thick corrosive atmosphere',
                'distance' => 1.557,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Topaz',
                'type' => 'World with indigenous life and oxygen atmosphere',
                'distance' => 2.343,
                'ports' => ['Camp Jameson'],
                'children' => [
                  {
                    'name' => 'Peters Base',
                    'type' => 'Orbital station',
                    'distance' => 0.0,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => "Peters's Wreck",
                'type' => 'Rocky planet with a thin atmosphere',
                'distance' => 3.497,
                'ports' => ['Fort Stevens'],
                'children' => [
                  {
                    'name' => 'Fortress York',
                    'type' => 'Orbital Station',
                    'distance' => 0.0,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => 'New America',
                'type' => 'Terraformed world with introduced life',
                'distance' => 5.258,
                'ports' => %w[Simpson Manchester],
                'children' => [
                  {
                    'name' => 'Patrick Depot',
                    'type' => 'Orbital station',
                    'distance' => 0.0,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => 'Facece 10',
                'type' => 'Large gas giant',
                'distance' => 8.107,
                'ports' => [],
                'children' => [
                  {
                    'name' => 'Facece 10a',
                    'type' => 'Barren rocky planetoid',
                    'distance' => 0.05,
                    'ports' => [],
                    'children' => [],
                  },
                  {
                    'name' => 'Facece 10b',
                    'type' => 'Barren rocky planetoid',
                    'distance' => 0.23,
                    'ports' => [],
                    'children' => [],
                  },
                  {
                    'name' => 'Facece 10c',
                    'type' => '',
                    'distance' => 0.36,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => 'Facece 11',
                'type' => 'Medium gas giant',
                'distance' => 17.439,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Facece 12',
                'type' => 'Rocky planet with a thin atmosphere',
                'distance' => 37.230,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Facece 13',
                'type' => 'Large gas giant',
                'distance' => 54.528,
                'ports' => [],
                'children' => [
                  {
                    'name' => 'Facece 13a',
                    'type' => 'Barren rocky planetoid',
                    'distance' => 0.06,
                    'ports' => [],
                    'children' => [],
                  },
                  {
                    'name' => 'Facece 13b',
                    'type' => 'Rocky planet with a thin atmosphere',
                    'distance' => 0.10,
                    'ports' => [],
                    'children' => [],
                  },
                  {
                    'name' => 'Facece 13c',
                    'type' => 'Barren rocky planetoid',
                    'distance' => 0.88,
                    'ports' => [],
                    'children' => [],
                  },
                ],
              },
              {
                'name' => 'Facece 14',
                'type' => 'Rocky planet with a thin atmosphere',
                'distance' => 78.892,
                'ports' => [],
                'children' => [],
              },
              {
                'name' => 'Facece 15',
                'type' => 'Medium gas giant',
                'distance' => 118.336,
                'ports' => [],
                'children' => [],
              },
            ],
          },
        ]
      }

      it { is_expected.to eq expected }
    end
  end
end
