require 'rails_helper'

RSpec.describe Route do
  let(:origin) { Star.lookup('Sol', 0, 0) }
  let(:destination) { Star.lookup('Achenar', 1, -4) }
  let(:route) { described_class.new(origin: origin, destination: destination) }

  describe '#wormholes' do
    subject { route.wormholes.map { |s| [s.name, s.sector_x, s.sector_y] } }

    it { # rubocop:todo RSpec/ExampleLength
      is_expected.to eq [
        ['Essexfa', 80, 16],
        ['Ineneth', -79, -21],
        ['Laaymi', 80, 16],
        ['Canbear', 80, 16],
        ['Datiqu', 80, 16],
        ['Essphibe', -79, -21],
        ['Arured', -80, -20],
        ['Arbeeth', -80, -19],
        ['Enandin', -80, -19],
        ['Waayze', 80, 16],
        ['Cetiqu', -79, -21],
      ]
    }
  end

  describe '#profits' do
    subject { route.profits }

    it { # rubocop:todo RSpec/ExampleLength
      is_expected.to eq [
        ['gem_stones', 78.6, 0.0, 0.0, 0],
        ['luxury_goods', 50.4, 0.504e2, 0.0, 0], # rubocop:todo Style/ExponentialNotation
        ['precious_metals', 7.3, 0.64e1, 0.162e2, 1], # rubocop:todo Style/ExponentialNotation
        ['medicines', 3.5, 0.38e1, 0.0, 0], # rubocop:todo Style/ExponentialNotation
        ['military_fuel', 2.4, 0.23e1, 0.25e1, 1], # rubocop:todo Style/ExponentialNotation
        ['computers', 0.8, 0.0, 0.0, 0],
        ['fertilizer', 0.6, 0.5e0, 0.5e0, 1], # rubocop:todo Style/ExponentialNotation
        ['hydrogen_fuel', 0.1, 0.1e0, 0.0, 0], # rubocop:todo Style/ExponentialNotation
        ['water', 0.0, 0.0, -0.1e0, -1], # rubocop:todo Style/ExponentialNotation
        ['rubbish', 0.0, 0.0, 0.0, 0],
        ['radioactives', -0.1, 0.0, 0.0, 0],
        ['synthetic_meat', -0.1, 0.3e0, 0.7e0, 1], # rubocop:todo Style/ExponentialNotation
        ['minerals', -0.2, 0.0, 0.0, 0],
        ['heavy_plastics', -0.3, 0.0, 0.0, 0],
        ['liquid_oxygen', -0.6, -0.6e0, -0.1e1, -1], # rubocop:todo Style/ExponentialNotation
        ['metal_alloys', -0.7, 0.0, 0.0, 0],
        ['grain', -1.4, -0.14e1, -0.33e1, -1], # rubocop:todo Style/ExponentialNotation
        ['air_processors', -1.8, 0.0, 0.0, 0],
        ['industrial_parts', -3.3, -0.16e1, 0.0, 0], # rubocop:todo Style/ExponentialNotation
        ['farm_machinery', -3.8, -0.38e1, -0.38e1, -1], # rubocop:todo Style/ExponentialNotation
        ['liquor', -11.4, -0.114e2, 0.0, 0], # rubocop:todo Style/ExponentialNotation
        ['fruit_and_veg', -12.7, -0.127e2, -0.146e2, -1], # rubocop:todo Style/ExponentialNotation
        ['nerve_gas', -14.3, 0.0, 0.0, 0],
        ['animal_meat', -19.1, -0.191e2, -0.214e2, -1], # rubocop:todo Style/ExponentialNotation
        ['robots', -34.4, -0.344e2, -0.49e2, -1], # rubocop:todo Style/ExponentialNotation
        ['hand_weapons', -446.2, -0.4473e3, -0.4473e3, -6], # rubocop:todo Style/ExponentialNotation
        ['battle_weapons', -593.6, -0.5935e3, -0.5935e3, -8], # rubocop:todo Style/ExponentialNotation
        ['narcotics', -691.3, -0.6935e3, -0.6935e3, -9], # rubocop:todo Style/ExponentialNotation
        ['animal_skins', -837.6, -0.8377e3, -0.8377e3, -9], # rubocop:todo Style/ExponentialNotation
        ['slaves', -849.2, -0.8288e3, -0.7929e3, -6], # rubocop:todo Style/ExponentialNotation
        ['live_animals', -1003.5, -0.9979e3, -0.9979e3, -9], # rubocop:todo Style/ExponentialNotation
      ]
    }
  end
end
