@startuml
title Andawa Gems

left to right direction
hide empty members
skinparam shadowing false
set namespaceSeparator none

legend top right
  <back:#f5f5f5>**(G)**</back> Gem sourced from GitHub
  <back:#cc342d>**(R)**</back> Requested (i.e. is in ""Gemfile""). Italic text shows requested version if present
  <back:#e59996>**(D)**</back> Dependency (i.e. is in ""Gemfile.lock"" but not ""Gemfile"")
  <back:#add1b2>**(C)**</back> Dependency present but not listed in ""Gemfile.lock"" (e.g. Bundler)
endlegend

"actioncable" -up-> "7.0.5" "actionpack"
"actioncable" -up-> "7.0.5" "activesupport"
"actioncable" -up-> "~> 2.0" "nio4r"
"actioncable" -up-> ">= 0.6.1" "websocket-driver"
"actionmailbox" -up-> "7.0.5" "actionpack"
"actionmailbox" -up-> "7.0.5" "activejob"
"actionmailbox" -up-> "7.0.5" "activerecord"
"actionmailbox" -up-> "7.0.5" "activestorage"
"actionmailbox" -up-> "7.0.5" "activesupport"
"actionmailbox" -up-> ">= 2.7.1" "mail"
"actionmailbox" -up-> "net-imap"
"actionmailbox" -up-> "net-pop"
"actionmailbox" -up-> "net-smtp"
"actionmailer" -up-> "7.0.5" "actionpack"
"actionmailer" -up-> "7.0.5" "actionview"
"actionmailer" -up-> "7.0.5" "activejob"
"actionmailer" -up-> "7.0.5" "activesupport"
"actionmailer" -up-> "~> 2.5, >= 2.5.4" "mail"
"actionmailer" -up-> "net-imap"
"actionmailer" -up-> "net-pop"
"actionmailer" -up-> "net-smtp"
"actionmailer" -up-> "~> 2.0" "rails-dom-testing"
"actionpack" -up-> "7.0.5" "actionview"
"actionpack" -up-> "7.0.5" "activesupport"
"actionpack" -up-> "~> 2.0, >= 2.2.4" "rack"
"actionpack" -up-> ">= 0.6.3" "rack-test"
"actionpack" -up-> "~> 2.0" "rails-dom-testing"
"actionpack" -up-> "~> 1.0, >= 1.2.0" "rails-html-sanitizer"
"actiontext" -up-> "7.0.5" "actionpack"
"actiontext" -up-> "7.0.5" "activerecord"
"actiontext" -up-> "7.0.5" "activestorage"
"actiontext" -up-> "7.0.5" "activesupport"
"actiontext" -up-> ">= 0.6.0" "globalid"
"actiontext" -up-> ">= 1.8.5" "nokogiri"
"actionview" -up-> "7.0.5" "activesupport"
"actionview" -up-> "~> 3.1" "builder"
"actionview" -up-> "~> 1.4" "erubi"
"actionview" -up-> "~> 2.0" "rails-dom-testing"
"actionview" -up-> "~> 1.1, >= 1.2.0" "rails-html-sanitizer"
"activejob" -up-> "7.0.5" "activesupport"
"activejob" -up-> ">= 0.3.6" "globalid"
"activemodel" -up-> "7.0.5" "activesupport"
"activerecord" -up-> "7.0.5" "activemodel"
"activerecord" -up-> "7.0.5" "activesupport"
"activestorage" -up-> "7.0.5" "actionpack"
"activestorage" -up-> "7.0.5" "activejob"
"activestorage" -up-> "7.0.5" "activerecord"
"activestorage" -up-> "7.0.5" "activesupport"
"activestorage" -up-> "~> 1.0" "marcel"
"activestorage" -up-> ">= 1.1.0" "mini_mime"
"activesupport" -up-> "~> 1.0, >= 1.0.2" "concurrent-ruby"
"activesupport" -up-> ">= 1.6, < 2" "i18n"
"activesupport" -up-> ">= 5.1" "minitest"
"activesupport" -up-> "~> 2.0" "tzinfo"
"debug" -up-> ">= 1.5.0" "irb"
"debug" -up-> ">= 0.3.1" "reline"
"globalid" -up-> ">= 5.0" "activesupport"
"i18n" -up-> "~> 1.0" "concurrent-ruby"
"irb" -up-> ">= 0.3.0" "reline"
"loofah" -up-> "~> 1.0.2" "crass"
"loofah" -up-> ">= 1.12.0" "nokogiri"
"mail" -up-> ">= 0.1.1" "mini_mime"
"mail" -up-> "net-imap"
"mail" -up-> "net-pop"
"mail" -up-> "net-smtp"
"net-imap" -up-> "date"
"net-imap" -up-> "net-protocol"
"net-pop" -up-> "net-protocol"
"net-protocol" -up-> "timeout"
"net-smtp" -up-> "net-protocol"
"nokogiri" -up-> "~> 1.4" "racc"
"parser" -up-> "~> 2.4.1" "ast"
"parser" -up-> "racc"
"pry" -up-> "~> 1.1" "coderay"
"pry" -up-> "~> 1.0" "method_source"
"pry-doc" -up-> "~> 0.11" "pry"
"pry-doc" -up-> "~> 0.9.11" "yard"
"pry-rails" -up-> ">= 0.10.4" "pry"
"puma" -up-> "~> 2.0" "nio4r"
"rack-test" -up-> ">= 1.3" "rack"
"rails" -up-> "7.0.5" "actioncable"
"rails" -up-> "7.0.5" "actionmailbox"
"rails" -up-> "7.0.5" "actionmailer"
"rails" -up-> "7.0.5" "actionpack"
"rails" -up-> "7.0.5" "actiontext"
"rails" -up-> "7.0.5" "actionview"
"rails" -up-> "7.0.5" "activejob"
"rails" -up-> "7.0.5" "activemodel"
"rails" -up-> "7.0.5" "activerecord"
"rails" -up-> "7.0.5" "activestorage"
"rails" -up-> "7.0.5" "activesupport"
"rails" -up-> ">= 1.15.0" "bundler"
"rails" -up-> "7.0.5" "railties"
"rails-dom-testing" -up-> ">= 4.2.0" "activesupport"
"rails-dom-testing" -up-> ">= 1.6" "nokogiri"
"rails-html-sanitizer" -up-> "~> 2.21" "loofah"
"rails-html-sanitizer" -up-> "~> 1.14" "nokogiri"
"railties" -up-> "7.0.5" "actionpack"
"railties" -up-> "7.0.5" "activesupport"
"railties" -up-> "method_source"
"railties" -up-> ">= 12.2" "rake"
"railties" -up-> "~> 1.0" "thor"
"railties" -up-> "~> 2.5" "zeitwerk"
"reline" -up-> "~> 0.5" "io-console"
"rspec-core" -up-> "~> 3.12.0" "rspec-support"
"rspec-expectations" -up-> ">= 1.2.0, < 2.0" "diff-lcs"
"rspec-expectations" -up-> "~> 3.12.0" "rspec-support"
"rspec-mocks" -up-> ">= 1.2.0, < 2.0" "diff-lcs"
"rspec-mocks" -up-> "~> 3.12.0" "rspec-support"
"rspec-rails" -up-> ">= 6.1" "actionpack"
"rspec-rails" -up-> ">= 6.1" "activesupport"
"rspec-rails" -up-> ">= 6.1" "railties"
"rspec-rails" -up-> "~> 3.12" "rspec-core"
"rspec-rails" -up-> "~> 3.12" "rspec-expectations"
"rspec-rails" -up-> "~> 3.12" "rspec-mocks"
"rspec-rails" -up-> "~> 3.12" "rspec-support"
"rubocop" -up-> "~> 2.3" "json"
"rubocop" -up-> "~> 1.10" "parallel"
"rubocop" -up-> ">= 3.2.0.0" "parser"
"rubocop" -up-> ">= 2.2.2, < 4.0" "rainbow"
"rubocop" -up-> ">= 1.8, < 3.0" "regexp_parser"
"rubocop" -up-> ">= 3.2.5, < 4.0" "rexml"
"rubocop" -up-> ">= 1.28.0, < 2.0" "rubocop-ast"
"rubocop" -up-> "~> 1.7" "ruby-progressbar"
"rubocop" -up-> ">= 2.4.0, < 3.0" "unicode-display_width"
"rubocop-ast" -up-> ">= 3.2.1.0" "parser"
"rubocop-capybara" -up-> "~> 1.41" "rubocop"
"rubocop-factory_bot" -up-> "~> 1.33" "rubocop"
"rubocop-performance" -up-> ">= 1.7.0, < 2.0" "rubocop"
"rubocop-performance" -up-> ">= 0.4.0" "rubocop-ast"
"rubocop-rails" -up-> ">= 4.2.0" "activesupport"
"rubocop-rails" -up-> ">= 1.1" "rack"
"rubocop-rails" -up-> ">= 1.33.0, < 2.0" "rubocop"
"rubocop-rspec" -up-> "~> 1.33" "rubocop"
"rubocop-rspec" -up-> "~> 2.17" "rubocop-capybara"
"rubocop-rspec" -up-> "~> 2.22" "rubocop-factory_bot"
"sprockets" -up-> "~> 1.0" "concurrent-ruby"
"sprockets" -up-> ">= 2.2.4, < 4" "rack"
"sprockets-rails" -up-> ">= 5.2" "actionpack"
"sprockets-rails" -up-> ">= 5.2" "activesupport"
"sprockets-rails" -up-> ">= 3.0.0" "sprockets"
"tzinfo" -up-> "~> 1.0" "concurrent-ruby"
"websocket-driver" -up-> ">= 0.1.0" "websocket-extensions"

class "actioncable" <<(D, #e59996)>> {
  7.0.5
}
class "actionmailbox" <<(D, #e59996)>> {
  7.0.5
}
class "actionmailer" <<(D, #e59996)>> {
  7.0.5
}
class "actionpack" <<(D, #e59996)>> {
  7.0.5
}
class "actiontext" <<(D, #e59996)>> {
  7.0.5
}
class "actionview" <<(D, #e59996)>> {
  7.0.5
}
class "activejob" <<(D, #e59996)>> {
  7.0.5
}
class "activemodel" <<(D, #e59996)>> {
  7.0.5
}
class "activerecord" <<(D, #e59996)>> {
  7.0.5
}
class "activestorage" <<(D, #e59996)>> {
  7.0.5
}
class "activesupport" <<(D, #e59996)>> {
  7.0.5
}
class "ast" <<(D, #e59996)>> {
  2.4.2
}
class "builder" <<(D, #e59996)>> {
  3.2.4
}
class "coderay" <<(D, #e59996)>> {
  1.1.3
}
class "concurrent-ruby" <<(D, #e59996)>> {
  1.2.2
}
class "crass" <<(D, #e59996)>> {
  1.0.6
}
class "date" <<(D, #e59996)>> {
  3.3.3
}
class "debug" <<(R, #cc342d)>> {
  1.8.0
}
class "diff-lcs" <<(D, #e59996)>> {
  1.5.0
}
class "erubi" <<(D, #e59996)>> {
  1.12.0
}
class "globalid" <<(D, #e59996)>> {
  1.1.0
}
class "i18n" <<(D, #e59996)>> {
  1.14.1
}
class "io-console" <<(D, #e59996)>> {
  0.6.0
}
class "irb" <<(D, #e59996)>> {
  1.7.0
}
class "json" <<(D, #e59996)>> {
  2.6.3
}
class "loofah" <<(D, #e59996)>> {
  2.21.3
}
class "mail" <<(D, #e59996)>> {
  2.8.1
}
class "marcel" <<(D, #e59996)>> {
  1.0.2
}
class "method_source" <<(D, #e59996)>> {
  1.0.0
}
class "mini_mime" <<(D, #e59996)>> {
  1.1.2
}
class "minitest" <<(D, #e59996)>> {
  5.18.0
}
class "net-imap" <<(D, #e59996)>> {
  0.3.4
}
class "net-pop" <<(D, #e59996)>> {
  0.1.2
}
class "net-protocol" <<(D, #e59996)>> {
  0.2.1
}
class "net-smtp" <<(D, #e59996)>> {
  0.3.3
}
class "nio4r" <<(D, #e59996)>> {
  2.5.9
}
class "nokogiri" <<(D, #e59996)>> {
  1.15.2-arm64-darwin
}
class "parallel" <<(D, #e59996)>> {
  1.23.0
}
class "parser" <<(D, #e59996)>> {
  3.2.2.3
}
class "pg" <<(R, #cc342d)>> {
  //~> 1.1//
  1.5.3
}
class "pry" <<(D, #e59996)>> {
  0.14.2
}
class "pry-doc" <<(R, #cc342d)>> {
  1.4.0
}
class "pry-rails" <<(R, #cc342d)>> {
  0.3.9
}
class "puma" <<(R, #cc342d)>> {
  //~> 5.0//
  5.6.5
}
class "racc" <<(D, #e59996)>> {
  1.7.0
}
class "rack" <<(D, #e59996)>> {
  2.2.7
}
class "rack-test" <<(D, #e59996)>> {
  2.1.0
}
class "rails" <<(R, #cc342d)>> {
  //~> 7.0.5//
  7.0.5
}
class "rails-dom-testing" <<(D, #e59996)>> {
  2.0.3
}
class "rails-html-sanitizer" <<(D, #e59996)>> {
  1.6.0
}
class "railties" <<(D, #e59996)>> {
  7.0.5
}
class "rainbow" <<(D, #e59996)>> {
  3.1.1
}
class "rake" <<(D, #e59996)>> {
  13.0.6
}
class "regexp_parser" <<(D, #e59996)>> {
  2.8.0
}
class "reline" <<(D, #e59996)>> {
  0.3.5
}
class "rexml" <<(D, #e59996)>> {
  3.2.5
}
class "rspec-core" <<(D, #e59996)>> {
  3.12.2
}
class "rspec-expectations" <<(D, #e59996)>> {
  3.12.3
}
class "rspec-mocks" <<(D, #e59996)>> {
  3.12.5
}
class "rspec-rails" <<(R, #cc342d)>> {
  6.0.3
}
class "rspec-support" <<(D, #e59996)>> {
  3.12.0
}
class "rubocop" <<(D, #e59996)>> {
  1.52.0
}
class "rubocop-ast" <<(D, #e59996)>> {
  1.29.0
}
class "rubocop-capybara" <<(D, #e59996)>> {
  2.18.0
}
class "rubocop-factory_bot" <<(D, #e59996)>> {
  2.23.1
}
class "rubocop-performance" <<(R, #cc342d)>> {
  1.18.0
}
class "rubocop-rails" <<(R, #cc342d)>> {
  2.19.1
}
class "rubocop-rspec" <<(R, #cc342d)>> {
  2.22.0
}
class "ruby-progressbar" <<(D, #e59996)>> {
  1.13.0
}
class "sprockets" <<(D, #e59996)>> {
  4.2.0
}
class "sprockets-rails" <<(R, #cc342d)>> {
  3.4.2
}
class "thor" <<(D, #e59996)>> {
  1.2.2
}
class "timeout" <<(D, #e59996)>> {
  0.3.2
}
class "tzinfo" <<(D, #e59996)>> {
  2.0.6
}
class "tzinfo-data" <<(R, #cc342d)>>
class "unicode-display_width" <<(D, #e59996)>> {
  2.4.2
}
class "websocket-driver" <<(D, #e59996)>> {
  0.7.5
}
class "websocket-extensions" <<(D, #e59996)>> {
  0.1.5
}
class "yard" <<(D, #e59996)>> {
  0.9.34
}
class "zeitwerk" <<(D, #e59996)>> {
  2.6.8
}

@enduml
