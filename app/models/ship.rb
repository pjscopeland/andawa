class Ship < ApplicationRecord
  belongs_to :default_drive, class_name: 'Drive'

  def self.current=(ship_name, drive = nil)
    @current_ship = find_by(name: ship_name)
    @current_drive = @current_ship.default_drive
    self.drive = drive if drive
    [@current_ship, @current_drive]
  end

  def self.drive=(klass, military: false)
    klass = 9 if klass == 8
    @current_drive = Drive.find_by(power: klass, military: military)
    @current_drive
  end

  def self.current
    [@current_ship, @current_drive]
  end

  def range_with(drive, hacked: false)
    # greater *or equal*, because if the drive fills the ship, there's no room for fuel
    return 0.0 if !hacked && drive.mass >= capacity

    # In the game, the charts list 0.00 when it would be less than 3. In actual fact, you can still use a drive that has
    # such a tiny range, so we don't put the same restriction on
    20_000 * (drive.power**2) / mass / 100.0
  end

  def fuel_for_jump(distance_ly)
    ((distance_ly % 655.36) * mass / 200.0).ceil
  end

  def start_decelerating_at(initial_distance)
    acceleration / (acceleration + deceleration) * initial_distance
  end
end
