class Route
  ATTRIBUTE_NAMES = [:origin, :destination, :distance, :wormholes, :profits]
  attr_accessor(*ATTRIBUTE_NAMES)

  MINIMUM_WORMHOLE_RANGE = 81.92 # sectors
  MAXIMUM_SHIP_RANGE = 54.54 / 8 # Sidewinder with class 3 drive

  def initialize(origin:, destination:)
    @origin = origin
    @destination = destination

    [:set_distance, :set_wormholes, :set_profits].each do |m|
      send m
    rescue => e
      warn "#{e.class}: #{e.message}\n#{e.backtrace.join("\n")}"
    end
  end

  def set_profits
    @profits = Stock::STOCK_TYPES.map { |t|
      buy = origin.stocks.send(t).first
      buy_for_actual = buy.price_decs
      buy_fine_estimate = Stock.send(t).where(status_code: buy.status_code).with_price.average(:price_decs)
      buy_rough_estimate = Stock.send(t).send(buy.status).with_price.average(:price_decs)

      sell = destination.stocks.send(t).first
      sell_for_actual = sell.price_decs
      sell_fine_estimate = Stock.send(t).where(status_code: sell.status_code).with_price.average(:price_decs)
      sell_rough_estimate = Stock.send(t).send(sell.status).with_price.average(:price_decs)

      [t] + [
        sell_for_actual && buy_for_actual && (sell_for_actual - buy_for_actual),
        sell_fine_estimate && buy_fine_estimate && (sell_fine_estimate - buy_fine_estimate),
        sell_rough_estimate && buy_rough_estimate && (sell_rough_estimate - buy_rough_estimate),
      ].map { |p| p&./(10.0)&.round(1) } +
        [sell.status_stand_in - buy.status_stand_in]
    }
    @profits.sort_by! { |_name, *diffs| -diffs.compact.first }
  end

  def set_distance
    self.distance = Frontier.distance(origin, destination) * 8
  end

  def set_wormholes
    all_candidates = wormhole_sector_coords.map { |x, y| Sector.find_or_create_by(x: x, y: y).stars }.flatten
    in_range_candidates = all_candidates.filter_map { |candidate|
      d1 = Frontier.distance(origin, candidate) % MINIMUM_WORMHOLE_RANGE
      d2 = Frontier.distance(candidate, destination) % MINIMUM_WORMHOLE_RANGE
      [candidate, d1, d2] if d1 < MAXIMUM_SHIP_RANGE && d2 < MAXIMUM_SHIP_RANGE
    }
    sorted_candidates = in_range_candidates.sort_by { |_star, d1, d2| [d1 + d2] }
    self.wormholes = sorted_candidates.each_with_object([]) { |(star, d1, d2), m|
      m << star
      break m if d1 > 0.125 && d2 > 0.125
    }
  end

  private

  def wormhole_sector_coords
    x1, y1 = origin.exact_coordinates
    x2, y2 = destination.exact_coordinates
    # direct distance
    d = Math.sqrt(((x2 - x1)**2) + ((y2 - y1)**2))
    # multiple of wormholes ranges needed to reach destination
    n = [1, (d / MINIMUM_WORMHOLE_RANGE).floor].max
    r1 = MINIMUM_WORMHOLE_RANGE
    r2 = n * MINIMUM_WORMHOLE_RANGE
    x3 = (
      (x1 + x2) +
      ((((r1**2) - (r2**2)) * (x2 - x1)) / (d**2)) +
      (Math.sqrt((2 * ((r1**2) + (r2**2)) / (d**2)) - ((((r1**2) - (r2**2))**2) / (d**4)) - 1) * (y2 - y1))
    ) / 2
    y3 = (
      (y1 + y2) +
      ((((r1**2) - (r2**2)) * (y2 - y1)) / (d**2)) +
      (Math.sqrt((2 * ((r1**2) + (r2**2)) / (d**2)) - ((((r1**2) - (r2**2))**2) / (d**4)) - 1) * (x1 - x2))
    ) / 2
    x4 = (
      (x1 + x2) +
      ((((r1**2) - (r2**2)) * (x2 - x1)) / (d**2)) -
      (Math.sqrt((2 * ((r1**2) + (r2**2)) / (d**2)) - ((((r1**2) - (r2**2))**2) / (d**4)) - 1) * (y2 - y1))
    ) / 2
    y4 = (
      (y1 + y2) +
      ((((r1**2) - (r2**2)) * (y2 - y1)) / (d**2)) -
      (Math.sqrt((2 * ((r1**2) + (r2**2)) / (d**2)) - ((((r1**2) - (r2**2))**2) / (d**4)) - 1) * (x1 - x2))
    ) / 2
    [[x3, y3], [x4, y4]].map { |x, y|
      (-1..1).map { |x_offset|
        (-1..1).map { |y_offset|
          [x.round + x_offset, y.round + y_offset]
        }
      }
    }.flatten(2)
  end
end
