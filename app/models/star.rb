require 'csv'

class Star < ApplicationRecord
  belongs_to :sector
  has_many :stocks, dependent: :destroy

  POPULATION_VALUES = [
    'None registered',
    'Less than 1,000',
    '1,000-10,000',
    '10,000-100,000',
    '100,000-1 Million',
    '1-10 Million',
    '10-100 Million',
    '100 Million-1 Billion',
    '1-10 Billion',
    'More than 10 Billion',
  ]

  enum :population, POPULATION_VALUES
  scope :populated, lambda { where('population > 0') }
  def populated? = population > 0

  def self.lookup(name, sec_x = nil, sec_y = nil)
    if /^(#{Sector::NAME_PART.join("|")}){3}$/i.match?(name)
      raise ArgumentError, 'Sector is required for generated names' if !sec_x || !sec_y

      sector_code = ((sec_x + Frontier::RELATIVE_X_OFFSET) << 13) + sec_y + Frontier::RELATIVE_Y_OFFSET
      if (star = Star.find_by('name = :name AND (galactic_id & 0x3ffffff) = :sector_code', name: name,
        sector_code: sector_code,))
        return star
      end
    else
      if (star = Star.find_by(name: name))
        return star
      end

      raise ArgumentError, "Sector is required for predefined stars that haven't been generated yet" if !sec_x || !sec_y

    end
    Sector.find_or_create_by(x: sec_x, y: sec_y).stars.find_by(name: name)
  end

  # Returns the star orthogonally closest to [0, 0] with this name. If it's a predefined name, there will only be one.
  def self.search(name)
    ones = [-1, 1]
    xys = [:x, :y]
    range = 0
    output = nil

    loop do
      sector = {x: range, y: range}
      return output if (output = Star.lookup(name, sector[:x], sector[:y]))

      if range >= 1
        ones.each { |one|
          xys.each { |xy|
            (range * 2).times do
              sector[xy] += one
              return output if (output = Star.lookup(name, sector[:x], sector[:y]))
            end
          }
        }
      end
      range += 1
    end
  end

  def self.generate_galactic_id(abs_x, abs_y, index) = (index << 26) + (abs_y << 13).+(abs_x)
  def sector_index = galactic_id >> (26)
  def sector_abs_x = galactic_id & 0x1fff
  def sector_abs_y = (galactic_id >> 13) & (0x1fff)
  def sector_x = sector_abs_x - Frontier::RELATIVE_X_OFFSET
  def sector_y = sector_abs_y - Frontier::RELATIVE_Y_OFFSET

  cattr_accessor :ext_atts
  def self.reload_ext_atts
    self.ext_atts = CSV.read('lib/star_attributes.csv').map { |row|
      row.map { |cell|
        Integer(cell.strip) rescue cell&.strip
      }
    }
  end
  reload_ext_atts
  EXT_ATT_NAMES = ext_atts[0]

  def ext_atts
    ext_att_values = self.class.ext_atts.find { |row| row[0] == galactic_id }
    distance = ((sector_x.abs + (sector_index & 7))**2) + ((sector_y.abs + (sector_index & 7))**2)
    ext_att_values ||= self.class.ext_atts[1..].find { |row|
      # !limit&.>(distance) means "distance is at least limit, or there is no limit"
      (row[1] || row[2]) && !row[1]&.>(distance) && !row[2]&.<=(distance)
    }

    EXT_ATT_NAMES.zip(ext_att_values).to_h
  end

  after_initialize do
    set_extended_attributes
    set_stocks
  end
  after_save do
    stocks.each(&:save)
  end

  def exact_coordinates
    [sector_x + (x / 128.0), sector_y + (y / 128.0), z / 128.0]
  end

  # def inspect
  #   "#{name} [#{sector_x}, #{sector_y}]"
  # end

  def ==(other)
    galactic_id == other.galactic_id
  end

  def set_extended_attributes
    self.population ||= Frontier::POPULATION_VALUES[ext_atts['population']]
    self.danger ||= ext_atts['danger']
    self.tech_level ||= ext_atts['tech_level']
    self.chance_cargo_check ||= ext_atts['chance_cargo_check']
    self.government ||= Frontier::GOVERNMENT_VALUES[ext_atts['government_allegiance'] & 0x3f]
    self.allegiance ||= Frontier::ALLEGIANCE_VALUES[(ext_atts['government_allegiance'] >> 6) & 7]
    self.federal_military_strength ||= ext_atts['fed_imp_check'] & 0xf
    self.imperial_military_strength ||= ext_atts['fed_imp_check'] & 0xf0
    self.short_description ||= Frontier::WORLD_DESC_VALUES[ext_atts['world_desc'] - 0x84e5]
    self.permit_required ||= !!(short_description =~ /Permit required./)
    self.long_description ||= Frontier::LONG_DESC_VALUES[ext_atts['long_desc'] - 0x8000]
  end

  def set_stocks
    return if ext_atts['population']&.to_i == 0

    Stock::STOCK_TYPES.each do |stock_type|
      stocks.send(stock_type).first_or_initialize(
        status_code: ext_atts["#{stock_type}_status"],
      ).attributes = ({
        price_decs: ext_atts["#{stock_type}_price"]&.to_i,
      })
    end
  end
end
