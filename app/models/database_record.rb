# frozen_string_literal: true

class DatabaseRecord < ApplicationRecord
  primary_abstract_class
end
