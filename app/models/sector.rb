require './lib/frontier'

class Sector < ApplicationRecord
  has_many :stars, dependent: :destroy

  validates :x, uniqueness: {scope: :y}
  NAME_PART = %w[en la can be and phi eth ol ve ho a lia an ar ur mi in ti qu so ed ess ex io ce ze fa ay wa da ack gre]

  after_initialize do
    abs_x, abs_y = Frontier.absolute(x, y)

    if (predefined_star_attributes = Frontier::PREDEFINED_STARS[[abs_x, abs_y]])
      predefined_star_attributes.each.with_index do |attributes, i|
        galactic_id = Star.generate_galactic_id(abs_x, abs_y, i)
        if (star = Star.find_by(galactic_id: galactic_id))
          stars | [star]
        else
          stars.build(galactic_id: galactic_id, **attributes)
        end
      end
    else
      number_of_stars = star_count

      Frontier.star_param_0 = ((abs_x << 16) + abs_y) & 0xFFFFFFFF
      Frontier.star_param_1 = ((abs_y << 16) + abs_x) & 0xFFFFFFFF
      3.times { Frontier.rotate_some }

      Array.new(number_of_stars) do |i|
        galactic_id = Star.generate_galactic_id(abs_x, abs_y, i)
        Frontier.rotate_some

        z = (Frontier.star_param_0 & 0xFF0000) >> 16
        z &= 0xFF
        z -= 0x100 if z > 0x7f
        y = (Frontier.star_param_0 >> 8)
        y &= 0xFF
        y -= 0x100 if y > 0x7f
        y /= 2
        y &= 0xFF
        y -= 0x100 if y > 0x7f
        x = (Frontier.star_param_0 & 0x0001FE) >> 1
        x &= 0xFF
        x -= 0x100 if x > 0x7f
        x /= 2
        x &= 0xFF
        x -= 0x100 if x > 0x7f
        multiple = Frontier::STAR_CHANCE_MULTIPLES[Frontier.star_param_1 & 0x1F]
        stardesc = Frontier::STAR_CHANCE_TYPE[(Frontier.star_param_1 >> 16) & 0x1F]
        name = star_name(i)
        stars.build(galactic_id: galactic_id, name: name, x: x, y: y, z: z, multiple: multiple, stardesc: stardesc)
      end
    end
  end

  after_save do
    stars.each(&:save)
  end

  def abs_x = x + Frontier::RELATIVE_X_OFFSET
  def abs_y = y + Frontier::RELATIVE_Y_OFFSET

  private

  def star_count
    x = abs_x
    y = abs_y
    return 0 if x > 0x1fff || y > 0x1fff

    # This is all magic code to generate pseudorandom numbers for the coordinates
    pixelval = (x / 64) + (2 * (y & 0x1fc0))
    p_1 = Frontier::MILKY_WAY[pixelval]
    p_2 = Frontier::MILKY_WAY[pixelval + 1]
    p_3 = Frontier::MILKY_WAY[pixelval + 128]
    p_4 = Frontier::MILKY_WAY[pixelval + 129]
    x = (x * 0x200) & 0x7e00
    y = (y * 0x200) & 0x7e00
    ebx = ((p_2 - p_1) * x) + ((p_3 - p_1) * y)
    ebx &= 0xFFFFFFFF
    esi = (x * y) >> 15
    edi = p_4 - p_3 - p_2 + p_1
    edi &= 0xFFFFFFFF
    esi *= edi
    esi &= 0xFFFFFFFF
    ebx += esi
    ebx &= 0xFFFFFFFF
    ebx += ebx
    ebx &= 0xFFFFFFFF
    p_1 <<= 16
    ebx += p_1
    ebx &= 0xFFFFFFFF
    ecx = ebx >> 8
    ebx = x + ecx
    eax = x * y
    eax >>= 15
    eax -= 0x100000000 if eax > 0x7FFFFFFF
    ebx ^= eax
    ebx >>= 5
    ebx -= 0x100000000 if ebx > 0x7FFFFFFF
    ebx &= 0x7f
    eax = Frontier::STAR_DENSITY[ebx]
    ecx *= eax
    ecx >>= 16
    p_1 = ecx
    p_1 >>= 10
    p_1 == 63 ? 62 : p_1
  end

  def star_name(sysnum)
    x = abs_x
    y = abs_y
    x += sysnum
    x &= 0xFFFF
    y += x
    y &= 0xFFFF
    x = Frontier.rotl(x, 3)
    x &= 0xFFFF
    x += y
    x &= 0xFFFF
    y = Frontier.rotl(y, 5)
    y &= 0xFFFF
    y += x
    y &= 0xFFFF
    y = Frontier.rotl(y, 4)
    x &= 0xFFFF
    x = Frontier.rotl(x, sysnum)
    x &= 0xFFFF
    x += y
    x &= 0xFFFF
    dest = NAME_PART[(x >> 2) & 31]
    x = Frontier.rotr(x, 5)
    x &= 0xFFFF
    dest += NAME_PART[(x >> 2) & 31]
    x = Frontier.rotr(x, 5)
    x &= 0xFFFF
    dest += NAME_PART[(x >> 2) & 31]
    dest.titlecase
  end
end
