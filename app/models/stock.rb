class Stock < ApplicationRecord
  belongs_to :star

  validates_presence_of :star, :status_code
  validates_uniqueness_of :stock_code, scope: :star
  # TYPES

  STOCK_TYPE_TEXTS = [
    'Water',
    'Liquid Oxygen',
    'Grain',
    'Fruit and Veg.',
    'Animal Meat',
    'Synthetic Meat',
    'Liquor',
    'Narcotics',
    'Medicines',
    'Fertilizer',
    'Animal Skins',
    'Live Animals',
    'Slaves',
    'Luxury Goods',
    'Heavy Plastics',
    'Metal Alloys',
    'Precious Metals',
    'Gem Stones',
    'Minerals',
    'Hydrogen Fuel',
    'Military Fuel',
    'Hand Weapons',
    'Battle Weapons',
    'Nerve Gas',
    'Industrial Parts',
    'Computers',
    'Air Processors',
    'Farm Machinery',
    'Robots',
    'Radioactives',
    'Rubbish',
    # 'Alien Artefacts', # ?! I think this is FFE only
    # 'Chaff', # ?! This is some sort of padding, and pretty sure FFE only
  ]
  STOCK_TYPES = STOCK_TYPE_TEXTS.map { |s| s.parameterize.underscore }
  enum :stock_code, STOCK_TYPES
  def stock_type = stock_code.to_sym
  def stock_type_text = STOCK_TYPES.zip(STOCK_TYPE_TEXTS).to_h[stock_code]

  # IMPORT / EXPORT STATUS

  STATUSES = {
    unlisted: [0, 1, 2, 8, 9, 10],
    minor_import: [3, 4],
    major_import: [5, 6, 7],
    minor_export: [11, 12],
    major_export: [13, 14, 15],
    illegal_goods: [129, 137],
  }
  STATUSES.each do |k, vs|
    scope k, lambda { where(status_code: vs) }
    define_method("#{k}?") { status == k }
  end
  def status = STATUSES.find { |k, v| break k if v.include? status_code }
  def status_text = status.to_s.tr('_', ' ').titlecase

  STATUS_PRICE_STAND_INS = [
    :major_export, :minor_export, :unlisted, :minor_import, :major_import,
    # Illegal Goods are MUCH more lucrative, so give them bigger spaces between other stocks
    nil, nil, nil, nil, nil, :illegal_goods,
  ]
  def status_stand_in = STATUS_PRICE_STAND_INS.index(status)

  # PRICES
  scope :with_price, lambda { where.not(price_decs: nil) }

  def price = price_decs / 10.0

  def price=(value)
    self.price_decs = (value * 10).to_i
  end

  def price_fine_estimate
    Stock.where(stock_code: stock_code, status_code: status_code).average(:price)
  end
end
