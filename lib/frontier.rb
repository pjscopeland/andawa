require 'csv'

module Frontier
  mattr_accessor :star_param_0, :star_param_1, instance_accessor: false
  RELATIVE_X_OFFSET = 5912
  RELATIVE_Y_OFFSET = 5412

  def self.absolute(rel_x, rel_y, rel_z = nil)
    [rel_x + RELATIVE_X_OFFSET, rel_y + RELATIVE_Y_OFFSET, rel_z].compact
  end

  def self.relative(abs_x, abs_y, abs_z = nil)
    [abs_x - RELATIVE_X_OFFSET, abs_y - RELATIVE_Y_OFFSET, abs_z].compact
  end

  def self.distance(star_1, star_2)
    Math.sqrt(star_1.exact_coordinates.zip(star_2.exact_coordinates).sum { |a, b| (a - b)**2 })
  end

  def self.rotate_some
    tmp_1 = (star_param_0 << 3) | (star_param_0 >> 29)
    tmp_1 &= 0xFFFFFFFF
    tmp_2 = star_param_0 + star_param_1
    tmp_2 &= 0xFFFFFFFF
    tmp_1 += tmp_2
    tmp_1 &= 0xFFFFFFFF
    self.star_param_0 = tmp_1
    self.star_param_1 = ((tmp_2 << 5) | (tmp_2 >> 27))
    self.star_param_1 &= 0xFFFFFFFF
  end

  def self.rotl(number, bits)
    ((number << bits) & 0xFFFF) | (number >> (16 - bits))
  end

  def self.rotr(number, bits)
    ((number << (16 - bits)) & 0xFFFF) | (number >> bits)
  end

  ALLEGIANCE_VALUES = %w[
    Independent
    Imperial
    Federation
    Alliance
    None
  ]

  COLOR_FOR_STAR = [
    0xc80000, 0xc80000, 0xc80000, 0xc88800, 0xc8c800, 0xc8c8c8, 0xc8c8c8, 0xc8c8c8, 0xc80000, 0xc8c8c8, 0xa8c8e8,
    0xc08800, 0xa8c8e8, 0xc8c800,
  ]

  GOVERNMENT_VALUES = [
    'None',
    'None (Anarchy)',
    'Rule by force of local Barons',
    'Policed by the local corporations',
    'Corporate State',
    'Federal Democracy',
    'Federal Colonial Rule',
    'Dictatorship',
    'Communist',
    'Religious Dictatorship',
    'Independent Democracy',
    'Imperial Rule',
    'Imperial Colonial Rule',
    'No Stable Government',
    'Martial Law',
    'Alliance Democracy',
  ]

  LONG_DESC_VALUES = [
    ' ',
    ' ',
    ' ',
    ' ',
    ' ',
    ' ',
    ' ',
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' ').sub("\r", "\n"),
      Famous as the first true corporate system. The Sirius corporation has been using the vast amounts of cheap energy
      available on Lucifer from coils wrapped around the planet for synthesis of custom elements (particularly military
      grade fuel) since 2350 and the first interstellar war.\rThis system is not subject to Federal law.
    STR
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' ').sub("\r", "\n"),
      Landfall has one of the most interesting skies of all the known habitable planets, with no less than six stars
      bright enough to cast shadows at some time.\rIt also has the best preserved indigenous wildlife which cohabits
      succesfully, often to the exclusion of introduced life.
    STR
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Very high humidity and native life make this an interesting place to visit. Many exotic food stuffs are grown
      here. The scenery is strikingly beautiful, though the wildlife can be a little dangerous.
    STR
    'Only just sustaining life, the indigenous forms on this arid planet are very hardy.',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      This system was given to the Guardians of the Free Spirit religious sect by the Federation in 2480. They believe
      in maximum hardship and live underground on several of the inner planets of the system. It is forbidden by Federal
      Law to enter the system without the permission of the Guardians.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Famous as the first flare star system to be settled, in 3017. All surface habitations have flare shelters beneath
      them, which should be entered on hearing the flare warnings. The first planet, often known as Lenin, was
      terraformed by a group of rich idealists on condition it was run to strict communist principles. They endowed an
      enormous amount of wealth to the system, which is now very rich.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' ').sub("\r", "\n"),
      Prisoners are housed in underground cell complexes, and set to do manual mining work. Visitors are allowed, but an
      entry permit must first be obtained.\r There is reputed to be a seedier side to the colony whereby lonely visitors
      may enter without a permit while the guards turn a blind eye for a fee, but this is always denied.
    STR
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Eden was the first planet on which liquid water was detected, by spectroscopic methods in 2038, and so was a major
      driving force for the exploration of space. However, Eden turned out to be extremely inhospitable with the added
      danger of hard radiation from Proxima. There is now a small research station on Eden, but little else in the rest
      of the system.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Vast areas are used for growing custom grain crops in enormous quantities which are then exported to the
      surrounding worlds.No manufacturing industry is permitted to avoid any polluting emmisions.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The inner planet was terraformed by the Cisco corporation into a beautiful theme park in 2958. The system does not
      come under Federal law, and a wide range of exotic delights are available, including historic adventures staged
      using actors.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The planet Scott was terraformed in 2329 as an agricultural colony, but the temperature could not be sustained and
      was abandoned in 2408 after a disease epidemic. The equatorial belt was resettled in 2641 by settlers who liked
      the harsh environment, now famous for fishing and tourism. It is particularly popular with the industrial workers
      from nearby systems who otherwise live their whole lives indoors.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Two planets here have now been terraformed, and both are largely given over to agricultural production and to
      ethically questionable cattle ranching. There is little manufacturing industry other than the production of the
      famous bourbon.
    STR
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      First colony outside the Sol system, and first alien life was discovered here. Unfortunately the last remnants are
      now in zoo enclosures for tourists, introduced life and crops cover most of the surface and seas.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Early life had just started to emerge on Arcturus 3 when the human settlers arrived in 2304. This has now been
      replaced by an Earth ecosystem, adapted to live under the red light. Locals are generally confident their star
      will remain a red giant forever.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Historic system famous as both the birthplace of Humanity and as the political capital of the Federation. A very
      expensive and prestigious system to live in, and a popular tourist venue. Most rich humans will visit Earth once
      in their lives. All the major corporations have their headquarters on Mars, terraformed in 2286, which is the main
      centre for administration.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Imperial capital on Achenar 6d, known as Capitol by the locals. 6c is still known as Conversion though it was
      terraformed in 2696. 6b was terraformed in 2850 to accomodate the population explosion which followed. Anyone
      without the quirky accent of Imperial citizens is shunned, especially if they are from a Federation world.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The system relies heavily on slavery to maintain the luxurious life style of the very wealthy élite. Most live on
      the great plantations, common throughout the Imperial Worlds. The cities house the workers for the service
      industries and the more exotic entertainments.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The deep and highly mechanised Imperial mines are renowned for their efficiency at ore extraction. Slavery is also
      important for the routine supervision of the machinery in the unpleasant mining environment.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Many Imperial worlds were terraformed during the last major Imperial expansion 2950-3150, including this, and
      there are numerous fine examples of architecture of the period.
    STR
    ' ',
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      There is no real law here, though in the cities organised crime does some form of policing. It is highly
      recommended to avoid this system if at all possible.
    STR
    ' ',
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      Well known Imperial Naval Base. Most Imperial naval administration is done on Topaz, and there are several outdoor
      training camps across the surface.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The main Federal Naval Base was moved here from Anlave in 2983 following terraforming in the 2970s, and is an
      excellent training ground for planetary assault troops.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      System most famous for the giant Verrix. Stevenson is the home world of the hardy beast of burden now found
      throughout human space, especially on the emerging worlds of the outer rim.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The Industrialised worlds of the Alliance are well known for the far better conditions available for workers than
      elsewhere.
    STR
    <<~STR.tr("\n", ' ').gsub(/  +/, ' '),
      The now famous capital of the Alliance of Independent Systems. This system was the subject of many battles between
      the Federation and the Empire over many years, until in 3230 the Alliance was formed in a massed insurrection
      against both sets of invaders.\rThe Alliance has now spread over many worlds, and so Alioth is now an important
      political centre.
    STR
    ' ',
    ' ',
    ' ',
    ' ',
  ]

  MILKY_WAY = CSV.read('lib/milky_way.csv').sum([]).map(&:to_i)

  POPULATION_VALUES = [
    'None registered',
    'Less than 1,000',
    '1,000-10,000',
    '10,000-100,000',
    '100,000-1 Million',
    '1-10 Million',
    '10-100 Million',
    '100 Million-1 Billion',
    '1-10 Billion',
    'More than 10 Billion',
  ]

  PREDEFINED_STARS = {
    [5912, 5412] => [ # 0, 0
      {name: 'Sol', x: 0, y: 16, z: -54, stardesc: 0x84, multiple: 0},
      {name: 'Alpha Centauri', x: -15, y: -52, z: -52, stardesc: 0x84, multiple: 2},
      {name: 'Wolf 359', x: 58, y: -19, z: -127, stardesc: 1, multiple: 0x18},
      {name: 'Lalande 21185', x: 47, y: 40, z: -127, stardesc: 1, multiple: 0xF8},
      {name: 'UV Ceti', x: 27, y: 38, z: 83, stardesc: 0, multiple: 1},
      {name: 'Ross 128', x: 48, y: -58, z: -127, stardesc: 1, multiple: 0xC8},
      {name: 'Tau Ceti', x: 41, y: 52, z: 127, stardesc: 4, multiple: 0x10},
      {name: 'LET 118', x: 14, y: 52, z: 127, stardesc: 1, multiple: 0xE0},
      {name: 'Wolf 424', x: 18, y: -56, z: -127, stardesc: 1, multiple: 0xB9},
      {name: 'CD-37º15492', x: -38, y: -24, z: 127, stardesc: 1, multiple: 0xE8},
      {name: 'Lalande 25372', x: -55, y: -36, z: -127, stardesc: 1, multiple: 0xC8},
    ],
    [5913, 5412] => [ # 1, 0
      {name: 'Sirius', x: 5, y: -17, z: -32, stardesc: 0x86, multiple: 1},
      {name: 'Epsilon Eridani', x: -19, y: 51, z: 72, stardesc: 3, multiple: 0x78},
      {name: 'Procyon', x: 49, y: 13, z: -94, stardesc: 5, multiple: 0x29},
      {name: 'BD+5º1668', x: 63, y: 18, z: -88, stardesc: 1, multiple: 0xE8},
      {name: 'BD+20º2465', x: 21, y: 5, z: -127, stardesc: 1, multiple: 0xF8},
    ],
    [5913, 5413] => [ # 1, 1
      {name: 'Groombridge 1618', x: -27, y: -6, z: -127, stardesc: 3, multiple: 0},
      {name: 'Luyten 1159-16', x: -59, y: 44, z: 123, stardesc: 1, multiple: 0},
      {name: 'WX Ursa Majoris', x: -37, y: -26, z: -127, stardesc: 0, multiple: 1},
      {name: '+53º1320', x: 26, y: 57, z: -127, stardesc: 1, multiple: 1},
    ],
    [5912, 5413] => [ # 0, 1
      {name: 'Ross 248', x: -36, y: 42, z: -8, stardesc: 1, multiple: 0x48},
      {name: 'Groombridge 34', x: -20, y: 63, z: 2, stardesc: 1, multiple: 0xEA},
      {name: 'van Maanens Star', x: -3, y: 9, z: 127, stardesc: 7, multiple: 0xA0},
      {name: 'Giclas 158-27', x: -45, y: -35, z: 127, stardesc: 1, multiple: 0x28},
    ],
    [5911, 5413] => [ # -1, 1
      {name: '61 Cygni', x: 11, y: 23, z: -37, stardesc: 0x83, multiple: 0x71},
      {name: 'Struve 2398', x: 35, y: 26, z: -127, stardesc: 2, multiple: 0xF1},
      {name: '1º4774', x: 47, y: 32, z: 127, stardesc: 1, multiple: 0x30},
    ],
    [5911, 5412] => [ # -1, 0
      {name: 'Barnards Star', x: 37, y: 12, z: -77, stardesc: 2, multiple: 8},
      {name: 'Ross 154', x: -11, y: -39, z: -26, stardesc: 0x82, multiple: 0xD0},
      {name: 'Luyten 789-6', x: 36, y: 40, z: 90, stardesc: 1, multiple: 0x58},
      {name: 'Lacaille 9352', x: 60, y: -18, z: 116, stardesc: 1, multiple: 0xF8},
      {name: 'Lacaille 8760', x: 2, y: -52, z: 85, stardesc: 1, multiple: 0xF8},
      {name: 'Ross 780', x: 7, y: 59, z: 127, stardesc: 1, multiple: 0xF0},
      {name: 'Fomalhaut', x: -16, y: -10, z: 127, stardesc: 6, multiple: 0x28},
    ],
    [5911, 5411] => [ # -1, -1
      {name: 'Epsilon Indi', x: 62, y: 45, z: 79, stardesc: 3, multiple: 0x50},
      {name: 'BD 4523', x: -38, y: 48, z: -127, stardesc: 1, multiple: 0xE1},
      {name: 'CD-46º11540', x: -26, y: -39, z: -24, stardesc: 1, multiple: 0xF8},
      {name: 'CD-49º13515', x: 6, y: 29, z: 122, stardesc: 1, multiple: 0xD8},
      {name: 'CD-44º11909', x: -37, y: -34, z: -24, stardesc: 1, multiple: 0xC8},
      {name: '-11º3759', x: -9, y: -52, z: -127, stardesc: 1, multiple: 0x80},
    ],
    [5912, 5411] => [], # 0, -1
    [5913, 5411] => [ # 1, -1
      {name: 'Kapteyns Star', x: 2, y: 46, z: 66, stardesc: 2, multiple: 0xF0},
      {name: '82 Eridani', x: 13, y: 36, z: 127, stardesc: 4, multiple: 0x40},
    ],
    [5914, 5411] => [ # 2, -1
      {name: 'Luyten 674-15', x: 11, y: 0, z: -87, stardesc: 1, multiple: 0x80},
    ],
    [5914, 5412] => [ # 2, 0
      {name: 'Ross 614', x: -48, y: 16, z: -31, stardesc: 1, multiple: 1},
      {name: 'Omicron Eridani', x: -61, y: 60, z: 102, stardesc: 4, multiple: 2},
      {name: 'YZ Canis Minoris', x: 32, y: 3, z: -119, stardesc: 0, multiple: 0},
      {name: '-21º1377', x: 17, y: -58, z: 42, stardesc: 2, multiple: 0xF8},
      {name: 'HD 36395', x: 31, y: 48, z: 48, stardesc: 2, multiple: 0xF8},
      {name: 'LP 658-2', x: 47, y: 32, z: 24, stardesc: 7, multiple: 0x78},
    ],
    [5914, 5413] => [ # 2, 1
      {name: 'Ross 986', x: -16, y: 49, z: -127, stardesc: 1, multiple: 0x18},
      {name: 'Ross 47', x: 32, y: -9, z: -4, stardesc: 1, multiple: 0x38},
      {name: 'Wolf 294', x: 3, y: 37, z: -127, stardesc: 1, multiple: 0xF8},
    ],
    [5913, 5414] => [ # 1, 2
      {name: 'Stein 2051', x: -14, y: 4, z: -90, stardesc: 2, multiple: 0x21},
    ],
    [5912, 5414] => [ # 0, 2
      {name: 'AC+79º3888', x: 14, y: -30, z: -127, stardesc: 1, multiple: 0xC8},
      {name: 'Eta Cassiopeia', x: -2, y: 63, z: -30, stardesc: 0x84, multiple: 1},
    ],
    [5911, 5414] => [ # -1, 2
      {name: 'Krüger 60', x: 63, y: -46, z: -56, stardesc: 2, multiple: 0x9A},
      {name: 'BD 946', x: 40, y: -46, z: -127, stardesc: 1, multiple: 0xC0},
      {name: 'BD+43º4305', x: 27, y: 4, z: 4, stardesc: 1, multiple: 0x90},
      {name: 'Sigma Draconis', x: 26, y: 13, z: -127, stardesc: 3, multiple: 0x88},
      {name: '+19º5116', x: 7, y: -3, z: 127, stardesc: 1, multiple: 0x19},
    ],
    [5910, 5413] => [ # -2, 1
      {name: 'Altair', x: 2, y: -46, z: -15, stardesc: 6, multiple: 0x10},
      {name: 'FU 46', x: 43, y: 54, z: -127, stardesc: 1, multiple: 0xB1},
    ],
    [5910, 5412] => [ # -2, 0
      {name: '70 Ophiuchi', x: -5, y: 0, z: -107, stardesc: 3, multiple: 0xAA},
      {name: 'VB 10', x: -54, y: 56, z: -38, stardesc: 2, multiple: 0xE9},
      {name: 'Arcturus', x: 60, y: -56, z: -127, stardesc: 8, multiple: 0},
    ],
    [5910, 5411] => [ # -2, -1
      {name: '36 Ophiuchi', x: 25, y: -16, z: -87, stardesc: 3, multiple: 0x72},
      {name: 'HR 7703', x: 32, y: 28, z: 97, stardesc: 3, multiple: 0x11},
      {name: 'Luyten 347-14', x: 49, y: -32, z: 67, stardesc: 1, multiple: 0xA0},
      {name: 'Wolf 630', x: -24, y: 29, z: -127, stardesc: 0x82, multiple: 0xEC},
    ],
    [5911, 5410] => [ # -1, -2
      {name: 'Delta Pavonis', x: 15, y: 49, z: 107, stardesc: 4, multiple: 0x70},
      {name: 'Luyten 205-128', x: -30, y: 21, z: 24, stardesc: 1, multiple: 8},
      {name: '-40º9712', x: -20, y: 9, z: -116, stardesc: 1, multiple: 0xF8},
      {name: '-45º13677', x: 46, y: -20, z: 119, stardesc: 1, multiple: 0x28},
    ],
    [5912, 5410] => [ # 0, -2
      {name: 'Luyten 145-41', x: 31, y: 22, z: -38, stardesc: 7, multiple: 0x48},
      {name: 'Beta Hydri', x: -8, y: 22, z: 127, stardesc: 4, multiple: 0x88},
    ],
    [5913, 5410] => [ # 1, -2
      {name: 'Luyten 97-12', x: -17, y: 10, z: 49, stardesc: 7, multiple: 0x50},
    ],
    [5909, 5414] => [ # -3, 2
      {name: 'Vega', x: 52, y: -20, z: -127, stardesc: 6, multiple: 0x68},
    ],
    [5917, 5414] => [ # 5, 2
      {name: 'Castor', x: 14, y: -63, z: -127, stardesc: 6, multiple: 0x15},
      {name: 'Pollux', x: -64, y: -38, z: -127, stardesc: 8, multiple: 0},
    ],
    [5917, 5411] => [ # 5, -1
      {name: 'Regulus', x: 59, y: -40, z: -127, stardesc: 0x0A, multiple: 1},
    ],
    [5913, 5408] => [ # 1, -4
      {name: 'Achenar', x: -4, y: -16, z: 127, stardesc: 0x0A, multiple: 0},
    ],
    [5916, 5416] => [ # 4, 4
      {name: 'Capella', x: -52, y: -36, z: -111, stardesc: 8, multiple: 3},
    ],
    [5918, 5416] => [ # 6, 4
      {name: 'Aldebaran', x: -1, y: -16, z: 127, stardesc: 8, multiple: 1},
    ],
    [5918, 5406] => [ # 6, -6
      {name: 'Canopus', x: -55, y: -9, z: 127, stardesc: 9, multiple: 0},
    ],
    [5908, 5395] => [ # -4, -17
      {name: 'Spica', x: -16, y: -12, z: -127, stardesc: 0x0A, multiple: 1},
    ],
    [5906, 5372] => [ # -6, -40
      {name: 'Hadar', x: -33, y: -48, z: -85, stardesc: 0x0C, multiple: 0},
    ],
    [5873, 5378] => [ # -39, -34
      {name: 'Antares', x: 37, y: -57, z: -127, stardesc: 0x0B, multiple: 1},
    ],
    [5971, 5426] => [ # 59, 14
      {name: 'Betelgeuse', x: -16, y: -32, z: 127, stardesc: 0x0B, multiple: 1},
    ],
    [6004, 5418] => [ # 92, 6
      {name: 'Rigel', x: 16, y: 48, z: 127, stardesc: 0x0C, multiple: 1},
    ],
    [5944, 5444] => [ # 32, 32
      {name: 'Alcyone', x: -16, y: 16, z: -54, stardesc: 9, multiple: 1},
      {name: 'Atlas', x: 56, y: 16, z: -54, stardesc: 0x0A, multiple: 1},
      {name: 'Electra', x: -56, y: 16, z: -46, stardesc: 0x0A, multiple: 0},
      {name: 'Maia', x: 32, y: 16, z: -102, stardesc: 0x0A, multiple: 0},
      {name: 'Merope', x: 16, y: 16, z: -22, stardesc: 0x0A, multiple: 0},
      {name: 'Taygete', x: 48, y: 16, z: -118, stardesc: 0x0A, multiple: 0},
      {name: 'Pleione', x: -59, y: 16, z: -62, stardesc: 0x0A, multiple: 0},
    ],
    [5912, 5488] => [ # 0, 76
      {name: 'Polaris', x: 52, y: -33, z: -127, stardesc: 9, multiple: 0},
    ],
    [5766, 5497] => [ # -146, 85
      {name: 'Beta Lyrae', x: -16, y: 16, z: -127, stardesc: 0x0D, multiple: 0},
    ],
    [5908, 5422] => [ # -4, 10
      {name: 'Alkaid', x: -19, y: -4, z: -127, stardesc: 0x0A, multiple: 0},
    ],
    [5912, 5417] => [ # 0, 5
      {name: 'Mizar', x: 16, y: 22, z: -127, stardesc: 6, multiple: 1},
      {name: 'Alcor', x: 18, y: 21, z: -127, stardesc: 5, multiple: 1},
    ],
    [5912, 5416] => [ # 0, 4
      {name: 'Alioth', x: -5, y: 56, z: -127, stardesc: 9, multiple: 0},
    ],
    [5913, 5418] => [ # 1, 6
      {name: 'Megrez', x: -24, y: 44, z: -127, stardesc: 6, multiple: 1},
    ],
    [5914, 5417] => [ # 2, 5
      {name: 'Phekda', x: -62, y: 6, z: -127, stardesc: 0x0A, multiple: 0},
    ],
    [5915, 5417] => [ # 3, 5
      {name: 'Merak', x: 44, y: -4, z: -127, stardesc: 6, multiple: 0},
    ],
    [5909, 5419] => [ # -3, 7
      {name: 'Dubhe', x: -31, y: -48, z: -127, stardesc: 9, multiple: 1},
    ],
    [5909, 5406] => [ # -3, -6
      {name: 'Lave', x: 48, y: 48, z: 0, stardesc: 0x83, multiple: 0},
      {name: 'Diso', x: -9, y: 45, z: 0, stardesc: 0x84, multiple: 0},
      {name: 'Riedquat', x: -60, y: 35, z: 0, stardesc: 0x84, multiple: 0},
      {name: 'Leesti', x: 3, y: 6, z: 0, stardesc: 0x82, multiple: 0},
      {name: 'Orerve', x: -3, y: -48, z: 0, stardesc: 4, multiple: 0},
    ],
    [5910, 5406] => [ # -2, -6
      {name: 'Zaonce', x: 3, y: 10, z: 0, stardesc: 0x85, multiple: 0},
      {name: 'Tionisla', x: 35, y: -16, z: 0, stardesc: 4, multiple: 0},
    ],
    [5909, 5407] => [ # -3, -5
      {name: 'Reorte', x: 47, y: -9, z: 0, stardesc: 4, multiple: 0},
      {name: 'Uszaa', x: -60, y: -16, z: 0, stardesc: 4, multiple: 0},
      {name: 'Orrere', x: -41, y: 16, z: 0, stardesc: 4, multiple: 0},
      {name: 'Quator', x: 10, y: 35, z: 0, stardesc: 4, multiple: 0},
    ],
    [5908, 5407] => [], # -4, -5
    [5908, 5406] => [], # -4, -6
    [5910, 5416] => [], # -2, 4
  }

  SIZE_FOR_STAR = [300, 300, 350, 400, 450, 500, 500, 200, 700, 900, 800, 1100, 1400, 600]

  STAR_CHANCE_MULTIPLES = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 3, 4, 5,
  ]

  STAR_CHANCE_TYPE = [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 6, 7, 8]

  STAR_DENSITY = [
    0xBF78, 0x5B2F, 0xDF85, 0x3C14, 0xDADD, 0x38DF, 0xE08F, 0x88D7, 0xB3AB, 0xEA86, 0x1200, 0x8DB3, 0xFF0D, 0xA593,
    0xEC66, 0x1988, 0x8500, 0xC1E7, 0x9281, 0xD7EB, 0x5F77, 0xD6A5, 0x310B, 0x2C98, 0x906E, 0x2CB6, 0xF137, 0x8ADC,
    0x0FC7, 0x76B8, 0xB587, 0x2D1B, 0xAD4C, 0x1AEB, 0xB749, 0xC60D, 0xB914, 0x1B3A, 0xAA5E, 0x3764, 0xD7A0, 0x650E,
    0xDB8D, 0x3E98, 0x1DDD, 0xD3BB, 0x54A4, 0x66BA, 0x164F, 0xF3B8, 0x7460, 0xBF9A, 0x7AA7, 0x459C, 0x61EC, 0xF706,
    0x958C, 0x8B54, 0x86E8, 0xC653, 0x5D7C, 0x6AC9, 0xAD35, 0x8B1F, 0x30C6, 0x7EF7, 0x4E4F, 0xD1F3, 0xD042, 0x4AAC,
    0x6F5A, 0x15C4, 0x4DC3, 0x923C, 0x04E2, 0x2C8B, 0xAB14, 0x9689, 0x5553, 0x92F7, 0x3BC6, 0x7C86, 0x5E8D, 0xFF7F,
    0x8F5C, 0x0450, 0x0BD3, 0xB01F, 0x2744, 0xDF20, 0xE40E, 0x932C, 0x8B90, 0xCF40, 0x6E2B, 0x81BE, 0x200B, 0xA64F,
    0x2BA4, 0xDCB8, 0xEA35, 0xACC4, 0x1421, 0x9025, 0x9A98, 0x4993, 0x99EF, 0xB4FD, 0x0BCF, 0x7434, 0x7287, 0xC67F,
    0x1967, 0xF486, 0x12AD, 0xDF33, 0xDF74, 0x2913, 0x2FF4, 0xD76B, 0x5A2A, 0x8B80, 0xCB01, 0x742B, 0x09B4, 0xC203,
    0x56AF, 0xDAD6, 0x8000, 0x5555, 0x4000, 0x3333, 0x2AAA, 0x2492, 0x1FF0, 0x1C71, 0x1999, 0x1745, 0x1555, 0x13B1,
    0x1249, 0x1111, 0x0FF0, 0x0F0F,
  ]

  STAR_DESC = [
    "Type'M'flare star", "Faint type'M'red star", "Type'M'red star", "Type'K'orange star", "Type'G'yellow star",
    "Type'F'white star", "Type'A'hot white star", 'White dwarf star', 'Red giant star', 'Bright giant star',
    "Type'B'hot blue star", 'Supergiant star', 'Blue supergiant star', 'Contact binary star',
  ]

  CELESTIAL_BODY_TYPES = [
    'Binary system',                                               # 0
    'Asteroidal body',                                             # 1
    'Small barren sphere of rock',                                 # 2
    'Barren rocky planetoid',                                      # 3
    'Rocky planet with a thin atmosphere',                         # 4
    'Highly volcanic world',                                       # 5
    'World with ammonia weather system and corrosive atmosphere',  # 6
    'World with methane weather system and corrosive atmosphere',  # 7
    'World with water weather system and corrosive atmosphere',    # 8
    'Small sustained terraformed world',                           # 9
    'World with indigenous life and oxygen atmosphere',            # 10
    'Terraformed world with introduced life',                      # 11
    'Rocky world with a thick corrosive atmosphere',               # 12
    'Small gas giant',                                             # 13
    'Medium gas giant',                                            # 14
    'Large gas giant',                                             # 15
    'Very large gas giant',                                        # 16
    'Brown dwarf substellar object',                               # 17
    "Type 'M' flare star",                                         # 18
    "Faint type 'M' red star",                                     # 19
    "Type 'M' red star",                                           # 20
    "Type 'K' orange star",                                        # 21
    "Type 'G' yellow star",                                        # 22
    "Type 'F' white star",                                         # 23
    "Type 'A' hot white star",                                     # 24
    'White dwarf star',                                            # 25
    'Red giant star',                                              # 26
    'Bright giant star',                                           # 27
    "Type 'B' hot blue star",                                      # 28
    'Supergiant star',                                             # 29
    'Blue supergiant star',                                        # 30
    'Contact binary star',                                         # 31
    'Orbital trading post',                                        # 32
    'Orbital station',                                             # 33
    'Orbital city',                                                # 34
    'City', # 35; added by [Jongware]
  ]

  WORLD_DESC_VALUES = [
    'Unexplored system. No more data available.',
    'System explored. No registered settlements.',
    'Frontier system. Some prospecting and mining.',
    'Some small scale mining operations.',
    'Mining and some ore refinement.',
    'Mining and heavy manufacturing industry.',
    'Extensive mining and industrial development.',
    'No Planets. No registered settlements.',
    'Rare element and reactor fuel production.',
    'Frontier outdoor world. Some farming and tourism.',
    'Outdoor agricultural world. Federation member.',
    'Outdoor ice and water world. Tourism and fishing.',
    'Outdoor jungle world. Tourism and agriculture.',
    'Outdoor desert world. Some agriculture.',
    'Isolationist religious enclave. Permit required.',
    'Strict communist regime. Permit required.',
    'Federal prison colony. Permit required.',
    'System under Federal interdict. Illegal to visit.',
    'Scientific research station.',
    'Terraformed agricultural world.',
    'Terraformed garden world. High-cost tourism.',
    'Terraformed ice and water world. Tourism.',
    'Two terraformed agricultural worlds. Tourism.',
    'High population outdoor world. Federation member.',
    'Historic Federal capital. Administration and Tourism.',
    'Imperial capital. Seat of the Emperor.',
    'High population outdoor world under Imperial rule.',
    'Imperial industrial and mining colony.',
    'Imperial terraformed world.',
    'System of special scientific interest.',
    'Outdoor world. Independent Dictatorship.',
    'Anarchic system. Minimal police protection.',
    'Disputed system.',
    'Imperial outdoor worlds and naval base.',
    'Federal naval base.',
    'Corporate ice world.',
    'Alliance Industrial and Mining System.',
    'Capital of the Alliance.',
    'High Population Alliance System.',
  ]
end
